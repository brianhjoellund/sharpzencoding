﻿namespace SharpZenCoding.Tests
{
    using Core;
    using NUnit.Framework;

    [TestFixture]
    public class ParserTests
    {
// ReSharper disable InconsistentNaming
        [Test]
        public void Expanding_A_Snippet_With_A_Tag_And_An_Id_Specifier_Should_Not_Result_In_A_Tag_With_Two_Id_Attributes()
        {
            var parser = new Parser();
            var result = parser.Parse("div#mainnav");

            Assert.That(result.Result, Is.EqualTo("<div id=\"mainnav\"></div>"));
        }

        [Test]
        public void Expanding_A_Snippet_With_A_Child_Selector_Should_Result_In_The_Latter_Tag_Being_A_Child_Of_The_Former()
        {
            var parser = new Parser();
            var result = parser.Parse("div>span");

            Assert.That(result.Result, Is.StringMatching(@"^\s*<div>\s*<span>\s*</span>\s*</div>\s*$"));
        }

        [Test]
        public void Given_An_Input_Of_A_Div_With_A_Child_Of_Span_The_SuggestedCaretOffset_Should_Be_On_The_Second_Line_And_Column_7()
        {
            var parser = new Parser();
            var result = parser.Parse("div>span");

            Assert.That(result.SuggestedCaretOffsets.Length, Is.EqualTo(1));
            Assert.That(result.SuggestedCaretOffsets[0].Row, Is.EqualTo(1));
            Assert.That(result.SuggestedCaretOffsets[0].Column, Is.EqualTo(7));
        }

        [Test]
        public void Given_An_Input_Of_A_Div_With_An_Id_Of_Test_And_A_Child_Of_Span_The_SuggestedCaretOffset_Should_Be_On_The_Second_Line_And_Column_7()
        {
            var parser = new Parser();
            var result = parser.Parse("div#test>span");

            Assert.That(result.SuggestedCaretOffsets.Length, Is.EqualTo(1));
            Assert.That(result.SuggestedCaretOffsets[0].Row, Is.EqualTo(1));
            Assert.That(result.SuggestedCaretOffsets[0].Column, Is.EqualTo(7));
        }

        [Test]
        public void Given_An_Input_Of_A_Div_With_Two_Children_Both_Of_Them_Spans_The_SuggestedCaretOffsets_Should_Be_On_The_Second_And_Third_Line_And_Both_At_Column_7()
        {
            var parser = new Parser();
            var result = parser.Parse("div>span+span");

            Assert.That(result.SuggestedCaretOffsets.Length, Is.EqualTo(2));
            Assert.That(result.SuggestedCaretOffsets[0].Row, Is.EqualTo(1));
            Assert.That(result.SuggestedCaretOffsets[0].Column, Is.EqualTo(7));
            Assert.That(result.SuggestedCaretOffsets[1].Row, Is.EqualTo(2));
            Assert.That(result.SuggestedCaretOffsets[1].Column, Is.EqualTo(7));
        }

        [Test]
        public void When_The_Index_Operator_Is_Used_The_Dollar_Sign_Is_Replaced_With_The_Current_Index()
        {
            var parser = new Parser();
            var result = parser.Parse("div>span#id-$*2");

            Assert.That(result.Result, Is.StringMatching(@"^<div>\s*<span id=""id-1""></span>\s*<span id=""id-2""></span>\s*</div>\s*$"));
        }
// ReSharper restore InconsistentNaming
    }
}
