﻿namespace SharpZenCoding.Core
{
    using SharpZenCoding.Abbrevations;
    using SharpZenCoding.Snippets;

    static class Providers
    {
        public static IAbbreviationProvider Abbrevations
        {
            get
            {
                return IoC.Get<IAbbreviationProvider>();
            }
        }

        public static ISnippetProvider Snippets
        {
            get
            {
                return IoC.Get<ISnippetProvider>();
            }
        }
    }
}
