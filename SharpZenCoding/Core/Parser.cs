﻿namespace SharpZenCoding.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System;
    using System.Text;
    using Serialization;

    class Parser
    {
        public ParseResults Parse(string zenCode, IEnumerable<string> input = null)
        {
            if(!Validate(zenCode))
                throw new ArgumentException("Invalid zen code");

            var state = ParserState.Snippet;

            IContext globalContext = new Context();
            
            if (input != null)
                globalContext.Input = input.ToArray();

            IContext context = globalContext;
            INode currentNode = new Node {Parent = globalContext};
            context.Children.Add(currentNode);

            for (int idx = 0; idx < zenCode.Length; idx++)
            {
                switch (state)
                {
                    case ParserState.Snippet:
                        currentNode.Name = GetSnippetName(ref idx, zenCode, ParserPredicates.IsLegalSnippetChar);
                        break;
                    case ParserState.Id:
                        currentNode.Id = GetId(ref idx, zenCode, ParserPredicates.IsLegalAttributeChar);
                        break;
                    case ParserState.Class:
                        currentNode.ClassNames.Add(GetClassName(ref idx, zenCode, ParserPredicates.IsLegalAttributeChar));
                        break;
                    case ParserState.Attributes:
                        var attributes = GetAttributes(ref idx, zenCode);

                        foreach (var attribute in attributes)
                            currentNode.Attributes[attribute.Key] = attribute.Value;
                        break;
                    case ParserState.Multiplier:
                        currentNode.Count = GetMultiplier(ref idx, zenCode, Char.IsDigit);
                        break;
                    case ParserState.New:
                        currentNode = new Node {Parent = context};
                        context.Children.Add(currentNode);
                        idx++;
                        break;
                    case ParserState.Child:
                        INode child = new Node {Parent = currentNode};
                        currentNode.Children.Add(child);
                        if(currentNode.ReceiveInput)
                        {
                            currentNode.ReceiveInput = false;
                            child.ReceiveInput = true;
                        }
                        context = currentNode;
                        currentNode = child;
                        idx++;
                        break;
                    case ParserState.Input:
                        if(input == null)
                            throw new ParseException("Found input token in zen code, but no input was specified");

                        currentNode.Count = globalContext.Input.Count();
                        currentNode.ReceiveInput = true;
                        idx++;
                        break;
                    case ParserState.Error:
                        throw new ParseException(String.Format("Unexpected char '{0}' found at position {1}", zenCode[idx], idx));
                }

                state = GetNextState(zenCode, idx);
                idx--;
            }

            string html = ConvertNodesToHtmlRepresentation(globalContext);
            Offset[] caretOffset = StripMarkerCharacterFromHtmlAndReturnOffsets(ref html);
            
            return new ParseResults
                       {
                           InputString = zenCode,
                           Result = html,
                           SuggestedCaretOffsets = caretOffset
                       };
        }

        private Offset[] StripMarkerCharacterFromHtmlAndReturnOffsets(ref string html)
        {
            if (html.Contains("$"))
            {
                var indices = new List<Offset>();

                var lines = Regex.Split(html, @"\r?\n");
                var processedLines = new List<string>();

                for (int index = 0; index < lines.Length; index++)
                {
                    var line = lines[index];
                    
                    while (line.Contains("$"))
                    {
                        var column = line.IndexOf("$", StringComparison.Ordinal);
                        line = line.Substring(0, column) + line.Substring(column + 1);
                        indices.Add(new Offset{Column = column, Row = index});
                    }

                    processedLines.Add(line);
                }

                html = String.Join("\r\n", processedLines);

                return indices.ToArray();
            }

            return new Offset[0];
        }

        private IEnumerable<KeyValuePair<string, string>> GetAttributes(ref int idx, string zenCode)
        {
            var attributes = new Dictionary<string, string>();

            idx++;

            while (true)
            {
                string property = GetString(ref idx, zenCode, (s, i) => s[i] != ']' && s[i] != '=' && !Char.IsWhiteSpace(s, i));
                string value = "";

                if(zenCode[idx] == '=')
                {
                    idx++;
                    switch(zenCode[idx])
                    {
                        case '"':
                            idx++;
                            value = GetString(ref idx, zenCode, (s, i) => s[i] != '"');
                            idx++;
                            break;
                        case '\'':
                            idx++;
                            value = GetString(ref idx, zenCode, (s, i) => s[i] != '\'');
                            idx++;
                            break;
                        default:
                            value = GetString(ref idx, zenCode, (s, i) => !Char.IsWhiteSpace(s[i]) && s[i] != ']');
                            break;
                    }
                }

                attributes.Add(property, value);

                while (Char.IsWhiteSpace(zenCode, idx))
                    idx++;

                if (zenCode[idx] == ']')
                {
                    idx++;
                    break;
                }
            }

            return attributes;
        }

        private static bool Validate(string zenCode)
        {
            return Regex.IsMatch(zenCode, @"^[A-Za-z][\w:-]*(?:\[(?:\w+(?:=(?:""[^""]*""|'[^']*'|\S+)\s*)?\s*)+\])?\+?(?:#[\w$-]+)?(?:\.[\w$-]+)*(?:\*\d*)?(?:(?:\+|>)[A-Za-z][\w:-]*(?:\[(?:\w+(?:=(?:""[^""]*""|'[^']*'|\S+)\s*)?\s*)+\])?\+?(?:#[\w$-]+)?(?:\.[\w$-]+)*(?:\*\d*)?)*$");
        }

        private static int GetMultiplier(ref int idx, string zenCode, Func<string, int, bool> isMultiplierChar)
        {
            idx++;
            string countString = GetString(ref idx, zenCode, isMultiplierChar);

            return Int32.Parse(countString);
        }

        private static string GetClassName(ref int idx, string zenCode, Func<string, int, bool> validate)
        {
            string className = GetString(ref idx, zenCode, validate);
            return className.Substring(1);
        }

        private static string GetId(ref int idx, string zenCode, Func<string, int, bool> validate)
        {
            string idString = GetString(ref idx, zenCode, validate);
            return idString.Substring(1);
        }

        private static string GetSnippetName(ref int idx, string zenCode, Func<string, int, bool> validate)
        {
            return GetString(ref idx, zenCode, validate);
        }

        private static string GetString(ref int idx, string zenCode, Func<string, int, bool> validate)
        {
            var sb = new StringBuilder();

            do
            {
                sb.Append(zenCode[idx]);
                idx++;
            } while (zenCode.Length > idx && validate(zenCode, idx));

            return sb.ToString();
        }

        private static ParserState GetNextState(string zenCode, int index)
        {
            if (zenCode.Length == index)
                return ParserState.Eof;

            if (zenCode[index] == '#')
                return ParserState.Id;

            if (zenCode[index] == '.')
                return ParserState.Class;

            if (zenCode[index] == '+')
                return ParserState.New;

            if (zenCode[index] == '*')
                if (zenCode.Length > index + 1 && Char.IsDigit(zenCode, index + 1))
                    return ParserState.Multiplier;
                else
                    return ParserState.Input;

            if (zenCode[index] == '>')
                return ParserState.Child;

            if (zenCode[index] == '[')
                return ParserState.Attributes;

            if (ParserPredicates.IsLegalChar(zenCode, index))
                return ParserState.Snippet;

            return ParserState.Error;
        }

        private static string ConvertNodesToHtmlRepresentation(IEnumerable<INode> context)
        {
            var sb = new StringBuilder();
            var serializer = IoC.Get<IOutputSerializer<string>>();

            foreach (var node in context)
                sb.Append(serializer.Serialize(node));

            return sb.ToString();
        }
    }
}