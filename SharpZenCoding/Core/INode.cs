namespace SharpZenCoding.Core
{
    using System.Collections.Generic;

    interface INode : IContext
    {
        string Name { get; set; }
        string Id { get; set; }
        IList<string> ClassNames { get; set; }
        IDictionary<string, string> Attributes { get; }
        int Count { get; set; }
        bool ReceiveInput { get; set; }
        IContext TopMostContext { get; }
    }
}