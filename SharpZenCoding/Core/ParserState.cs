namespace SharpZenCoding.Core
{
    enum ParserState
    {
        Snippet,
        Id,
        Class,
        New,
        Error,
        Eof,
        Multiplier,
        Child,
        Input,
        Attributes
    }
}