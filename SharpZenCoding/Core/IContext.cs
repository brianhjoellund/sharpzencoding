﻿namespace SharpZenCoding.Core
{
    using System.Collections.Generic;

    interface IContext : IEnumerable<INode>
    {
        IList<INode> Children { get; }
        IContext Parent { get; set; }
        IEnumerable<string> Input { get; set; }
    }
}
