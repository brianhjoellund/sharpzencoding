namespace SharpZenCoding.Core
{
    using System.Collections;
    using System.Collections.Generic;

    class Context : IContext
    {
        private readonly IList<INode> children = new List<INode>();

        public IList<INode> Children
        {
            get { return children; }
        }

        public IContext Parent
        {
            get { return null; }
            set { }
        }

        public IEnumerable<string> Input { get; set; }

        public IEnumerator<INode> GetEnumerator()
        {
            return Children.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}