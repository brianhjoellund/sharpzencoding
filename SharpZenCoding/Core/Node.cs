﻿namespace SharpZenCoding.Core
{
    using System.Collections;
    using System.Collections.Generic;

    class Node : INode
    {
        private readonly IDictionary<string, string> attributes = new Dictionary<string, string>();
        private string id;
        private IList<string> classNames = new List<string>();
        private IList<INode> children = new List<INode>();

        public IContext Parent { get; set; }
        public IList<INode> Children
        {
            get { return children; }
            set { children = value; }
        }

        public IEnumerable<string> Input { get; set; }

        public string Name { get; set; }
        public string Id
        {
            get { return id; } 
            set { id = value; }
        }
        
        public IList<string> ClassNames
        {
            get { return classNames; }
            set { classNames = value; }
        }

        public IDictionary<string, string> Attributes
        {
            get { return attributes; }
        }

        public int Count { get; set; }
        public bool ReceiveInput { get; set; }

        public IContext TopMostContext
        {
            get
            {
                IContext context = this;

                do
                {
                    if (context.Parent == null)
                        return context;

                    context = context.Parent;
                } while (true);
            }
        }

        public IEnumerator<INode> GetEnumerator()
        {
            return Children.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
