﻿namespace SharpZenCoding.Core
{
    public class ParseResults
    {
        public string InputString { get; set; }
        public string Result { get; set; }
        public Offset[] SuggestedCaretOffsets { get; set; }
    }

    public class Offset
    {
        public int Row { get; set; }
        public int Column { get; set; }
    }
}
