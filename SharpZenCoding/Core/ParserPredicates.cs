﻿namespace SharpZenCoding.Core
{
    using System;

    static class ParserPredicates
    {
        public static bool IsLegalAttributeChar(string zenCode, int index)
        {
            if (zenCode.Length > index)
                return IsLegalChar(zenCode, index) || zenCode[index] == '-' || zenCode[index] == '$';

            return false;
        }

        public static bool IsLegalSnippetChar(string zenCode, int index)
        {
            if (zenCode.Length > index)
                return IsLegalChar(zenCode, index) || zenCode[index] == ':' || (zenCode[index] == '+' && !IsLegalChar(zenCode, index + 1));

            return false;
        }

        public static bool IsLegalChar(string zenCode, int index)
        {
            if (zenCode.Length > index)
                return Char.IsLetterOrDigit(zenCode, index) || zenCode[index] == '_';

            return false;
        }
    }
}
