namespace SharpZenCoding.Abbrevations
{
    using System.Collections.Generic;

    class AbbreviationProvider : Dictionary<string, string>, IAbbreviationProvider
    {
        public AbbreviationProvider()
        {
            SetupRootElements();
            SetupDocumentMetaData();
            SetupScripting();
            SetupSections();
            SetupGroupingContent();
            SetupEdits();
            SetupEmbeddedContent();
            SetupTabularData();
            SetupForms();
            SetupInteractiveElements();
            SetupConditionalComments();
        }

        private void SetupConditionalComments()
        {
            Add("cc:ie", "<!--[if IE]>${child}<![endif]-->");
            Add("cc:noie", "<!--[if !IE]><!-->${child}<!--<![endif]-->");
        }

        private void SetupInteractiveElements()
        {
            Add("datagrid", AbbrevationGenerator.Tag("datagrid"));
            Add("datag", AbbrevationGenerator.Tag("datagrid"));
            Add("datalist", AbbrevationGenerator.Tag("datalist"));
            Add("datal", AbbrevationGenerator.Tag("datalist"));
            Add("textarea", AbbrevationGenerator.Tag("textarea", new { cols = "30", rows = "10" }));
            Add("keygen", AbbrevationGenerator.SelfClosedTag("keygen"));
            Add("output", AbbrevationGenerator.Tag("output"));
            Add("out", AbbrevationGenerator.Tag("output"));
            Add("details", AbbrevationGenerator.Tag("details"));
            Add("det", AbbrevationGenerator.Tag("details"));
            Add("command", AbbrevationGenerator.SelfClosedTag("command"));
            Add("cmd", AbbrevationGenerator.SelfClosedTag("command"));
            Add("bb", AbbrevationGenerator.Tag("bb"));
            Add("menu", AbbrevationGenerator.Tag("menu"));
            Add("menu:context", AbbrevationGenerator.Tag("menu", new {type = "context"}));
            Add("menu:toolbar", AbbrevationGenerator.Tag("menu", new {type = "toolbar"}));
        }

        private void SetupForms()
        {
            Add("form", AbbrevationGenerator.Tag("form", new {action = ""}));
            Add("form:get", AbbrevationGenerator.Tag("form", new {action = "", method = "get"}));
            Add("form:post", AbbrevationGenerator.Tag("form", new {action = "", method = "post"}));
            Add("fieldset", AbbrevationGenerator.Tag("fieldset"));
            Add("fset", AbbrevationGenerator.Tag("fieldset"));
            Add("legend", AbbrevationGenerator.Tag("legend"));
            Add("leg", AbbrevationGenerator.Tag("legend"));
            Add("label", AbbrevationGenerator.Tag("label", new{@for=""}));
            Add("input", AbbrevationGenerator.SelfClosedTag("input", new{type=""}));
            Add("input:hidden", AbbrevationGenerator.SelfClosedTag("input", new { type = "hidden", value = "" }));
            Add("input:h", AbbrevationGenerator.SelfClosedTag("input", new { type = "hidden", value = "" }));
            Add("input:text", AbbrevationGenerator.SelfClosedTag("input", new { type = "text", value = "" }));
            Add("input:t", AbbrevationGenerator.SelfClosedTag("input", new { type = "text", value = "" }));
            Add("input:search", AbbrevationGenerator.SelfClosedTag("input", new { type = "search", value = "" }));
            Add("input:email", AbbrevationGenerator.SelfClosedTag("input", new { type = "email", value = "" }));
            Add("input:url", AbbrevationGenerator.SelfClosedTag("input", new { type = "url", value = "" }));
            Add("input:password", AbbrevationGenerator.SelfClosedTag("input", new { type = "password", value = "" }));
            Add("input:p", AbbrevationGenerator.SelfClosedTag("input", new { type = "password", value = "" }));
            Add("input:datetime", AbbrevationGenerator.SelfClosedTag("input", new { type = "datetime", value = "" }));
            Add("input:datetime-local", AbbrevationGenerator.SelfClosedTag("input", new { type = "datetime-local", value = "" }));
            Add("input:date", AbbrevationGenerator.SelfClosedTag("input", new { type = "date", value = "" }));
            Add("input:month", AbbrevationGenerator.SelfClosedTag("input", new { type = "month", value = "" }));
            Add("input:week", AbbrevationGenerator.SelfClosedTag("input", new { type = "week", value = "" }));
            Add("input:time", AbbrevationGenerator.SelfClosedTag("input", new { type = "time", value = "" }));
            Add("input:number", AbbrevationGenerator.SelfClosedTag("input", new { type = "number", value = "" }));
            Add("input:range", AbbrevationGenerator.SelfClosedTag("input", new { type = "range", value = "" }));
            Add("input:color", AbbrevationGenerator.SelfClosedTag("input", new { type = "color", value = "" }));
            Add("input:checkbox", AbbrevationGenerator.SelfClosedTag("input", new { type = "checkbox" }));
            Add("input:c", AbbrevationGenerator.SelfClosedTag("input", new { type = "checkbox" }));
            Add("input:radio", AbbrevationGenerator.SelfClosedTag("input", new { type = "radio" }));
            Add("input:r", AbbrevationGenerator.SelfClosedTag("input", new { type = "radio" }));
            Add("input:file", AbbrevationGenerator.SelfClosedTag("input", new { type = "file" }));
            Add("input:f", AbbrevationGenerator.SelfClosedTag("input", new { type = "file" }));
            Add("input:submit", AbbrevationGenerator.SelfClosedTag("input", new { type = "submit", value = "" }));
            Add("input:s", AbbrevationGenerator.SelfClosedTag("input", new { type = "submit", value = "" }));
            Add("input:image", AbbrevationGenerator.SelfClosedTag("input", new { type = "image", src = "", alt = "" }));
            Add("input:i", AbbrevationGenerator.SelfClosedTag("input", new { type = "image", src = "", alt = "" }));
            Add("input:reset", AbbrevationGenerator.SelfClosedTag("input", new { type = "reset", value = "" }));
            Add("input:button", AbbrevationGenerator.SelfClosedTag("input", new { type = "button", value = "" }));
            Add("input:b", AbbrevationGenerator.SelfClosedTag("input", new { type = "button", value = "" }));
            Add("button", AbbrevationGenerator.Tag("button"));
            Add("btn", AbbrevationGenerator.Tag("button"));
            Add("select", AbbrevationGenerator.Tag("select"));
            Add("select+", "<select${id}${class}><option value=\"\"></option></select>${child}");
            Add("optgroup", AbbrevationGenerator.Tag("optgroup"));
            Add("optg", AbbrevationGenerator.Tag("optgroup"));
            Add("optgroup+", "<optgroup${id}${class}><option></option></optgroup>");
            Add("optg+", "<optgroup${id}${class}><option></option></optgroup>");
            Add("option", AbbrevationGenerator.Tag("option"));
            Add("opt", AbbrevationGenerator.Tag("option"));
        }

        private void SetupTabularData()
        {
            Add("table", AbbrevationGenerator.Tag("table"));
            Add("table+", "<table${id}${class}><tr><td></td></tr></table>${child}");
            Add("caption", AbbrevationGenerator.Tag("caption"));
            Add("cap", AbbrevationGenerator.Tag("caption"));
            Add("colgroup", AbbrevationGenerator.Tag("colgroup"));
            Add("colg", AbbrevationGenerator.Tag("colgroup"));
            Add("colgroup+", "<colgroup${id}${class}><col></colgroup>${child}");
            Add("colg+", "<colgroup${id}${class}><col></colgroup>${child}");
            Add("col", AbbrevationGenerator.SelfClosedTag("col"));
            Add("tbody", AbbrevationGenerator.Tag("tbody"));
            Add("thead", AbbrevationGenerator.Tag("thead"));
            Add("tfoot", AbbrevationGenerator.Tag("tfoot"));
            Add("tr", AbbrevationGenerator.Tag("tr"));
            Add("tr+", "<tr${id}${class}><td>${child}</td></tr>");
            Add("th", AbbrevationGenerator.Tag("th"));
            Add("td", AbbrevationGenerator.Tag("td"));
        }

        private void SetupEmbeddedContent()
        {
            Add("figure", AbbrevationGenerator.Tag("figure"));
            Add("img", AbbrevationGenerator.SelfClosedTag("img", new {src = "", alt = ""}));
            Add("iframe", AbbrevationGenerator.Tag("iframe", new {src = "", frameborder = "0"}));
            Add("embed", AbbrevationGenerator.Tag("embed", new {src = "", type = ""}));
            Add("emb", AbbrevationGenerator.Tag("embed", new {src = "", type = ""}));
            Add("object", AbbrevationGenerator.Tag("object", new {data = "", type = ""}));
            Add("obj", AbbrevationGenerator.Tag("object", new { data = "", type = "" }));
            Add("param", AbbrevationGenerator.SelfClosedTag("param"));
            Add("video", AbbrevationGenerator.Tag("video", new {src = ""}));
            Add("audio", AbbrevationGenerator.Tag("audio", new {src = ""}));
            Add("source", AbbrevationGenerator.SelfClosedTag("source"));
            Add("src", AbbrevationGenerator.SelfClosedTag("source"));
            Add("canvas", AbbrevationGenerator.Tag("canvas"));
            Add("map", AbbrevationGenerator.Tag("map", new {name = ""}));
            Add("map+", "<map name=\"\"${id}${class}><area shape=\"\"> coords=\"\" href=\"\" alt=\"\"></map>${child}");
            Add("area", AbbrevationGenerator.SelfClosedTag("area", new { shape = "", coords = "", href = "", alt = "" }));
            Add("area:d", AbbrevationGenerator.SelfClosedTag("area", new { shape = "default", coords = "", href = "", alt = "" }));
            Add("area:c", AbbrevationGenerator.SelfClosedTag("area", new { shape = "circle", coords = "", href = "", alt = "" }));
            Add("area:r", AbbrevationGenerator.SelfClosedTag("area", new { shape = "rect", coords = "", href = "", alt = "" }));
            Add("area:p", AbbrevationGenerator.SelfClosedTag("area", new { shape = "poly", coords = "", href = "", alt = "" }));
        }

        private void SetupEdits()
        {
            Add("ins", AbbrevationGenerator.Tag("ins"));
            Add("del", AbbrevationGenerator.Tag("del"));
        }

        private void SetupGroupingContent()
        {
            Add("p", AbbrevationGenerator.Tag("p"));
            Add("hr", AbbrevationGenerator.SelfClosedTag("hr"));
            Add("br", AbbrevationGenerator.SelfClosedTag("br"));
            Add("pre", AbbrevationGenerator.Tag("pre"));
            Add("dialog", AbbrevationGenerator.Tag("dialog"));
            Add("dlg", AbbrevationGenerator.Tag("dialog"));
            Add("blockquote", AbbrevationGenerator.Tag("blockquote"));
            Add("bq", AbbrevationGenerator.Tag("blockquote"));
            Add("ol", AbbrevationGenerator.Tag("ol"));
            Add("ol+", "<ol${id}${class}><li></li></ol>${child}");
            Add("ul", AbbrevationGenerator.Tag("ul"));
            Add("ul+", "<ul${id}${class}><li></li></ul>${child}");
            Add("li", AbbrevationGenerator.Tag("li"));
            Add("dl", AbbrevationGenerator.Tag("dl"));
            Add("dl+", "<dl${id}${class}><dt></dt><dd></dd></dl>${child}");
            Add("dt", AbbrevationGenerator.Tag("dt"));
            Add("dd", AbbrevationGenerator.Tag("dd"));
            Add("a", AbbrevationGenerator.Tag("a", new {href = ""}));
            Add("a:link", AbbrevationGenerator.Tag("a", new {href = "http://"}));
            Add("a:mailto", AbbrevationGenerator.Tag("a", new {href = "mailto:"}));
            Add("q", AbbrevationGenerator.Tag("q"));
            Add("cite", AbbrevationGenerator.Tag("cite"));
            Add("em", AbbrevationGenerator.Tag("em"));
            Add("strong", AbbrevationGenerator.Tag("strong"));
            Add("str", AbbrevationGenerator.Tag("str"));
            Add("small", AbbrevationGenerator.Tag("small"));
            Add("mark", AbbrevationGenerator.Tag("mark"));
            Add("dfn", AbbrevationGenerator.Tag("dfn"));
            Add("abbr", AbbrevationGenerator.Tag("abbr", new {title = ""}));
            Add("acronym", AbbrevationGenerator.Tag("acronym", new {title = ""}));
            Add("acr", AbbrevationGenerator.Tag("acronym", new {title = ""}));
            Add("time", AbbrevationGenerator.Tag("time"));
            Add("progress", AbbrevationGenerator.Tag("progress"));
            Add("prog", AbbrevationGenerator.Tag("progress"));
            Add("meter", AbbrevationGenerator.Tag("meter"));
            Add("code", AbbrevationGenerator.Tag("code"));
            Add("var", AbbrevationGenerator.Tag("var"));
            Add("samp", AbbrevationGenerator.Tag("samp"));
            Add("kbd", AbbrevationGenerator.Tag("kbd"));
            Add("sub", AbbrevationGenerator.Tag("sub"));
            Add("sup", AbbrevationGenerator.Tag("sup"));
            Add("span", AbbrevationGenerator.Tag("span"));
            Add("i", AbbrevationGenerator.Tag("i"));
            Add("b", AbbrevationGenerator.Tag("b"));
            Add("bdo", AbbrevationGenerator.Tag("bdo", new {dir = ""}));
            Add("bdo:r", AbbrevationGenerator.Tag("bdo", new {dir = "rtl"}));
            Add("bdo:l", AbbrevationGenerator.Tag("bdo", new {dir = "ltr"}));
            Add("ruby", AbbrevationGenerator.Tag("ruby"));
            Add("rt", AbbrevationGenerator.Tag("rt"));
            Add("rp", AbbrevationGenerator.Tag("rp"));
        }

        private void SetupSections()
        {
            Add("body", AbbrevationGenerator.Tag("body"));
            Add("section", AbbrevationGenerator.Tag("section"));
            Add("sect", AbbrevationGenerator.Tag("section"));
            Add("nav", AbbrevationGenerator.Tag("nav"));
            Add("article", AbbrevationGenerator.Tag("article"));
            Add("art", AbbrevationGenerator.Tag("article"));
            Add("aside", AbbrevationGenerator.Tag("aside"));
            Add("h1", AbbrevationGenerator.Tag("h1"));
            Add("h2", AbbrevationGenerator.Tag("h2"));
            Add("h3", AbbrevationGenerator.Tag("h3"));
            Add("h4", AbbrevationGenerator.Tag("h4"));
            Add("h5", AbbrevationGenerator.Tag("h5"));
            Add("h6", AbbrevationGenerator.Tag("h6"));
            Add("hgroup", AbbrevationGenerator.Tag("hgroup"));
            Add("hgr", AbbrevationGenerator.Tag("hgroup"));
            Add("header", AbbrevationGenerator.Tag("header"));
            Add("hdr", AbbrevationGenerator.Tag("header"));
            Add("footer", AbbrevationGenerator.Tag("footer"));
            Add("ftr", AbbrevationGenerator.Tag("footer"));
            Add("address", AbbrevationGenerator.Tag("address"));
            Add("adr", AbbrevationGenerator.Tag("address"));
            Add("div", AbbrevationGenerator.Tag("div"));
        }

        private void SetupScripting()
        {
            Add("script", AbbrevationGenerator.Tag("script", new {type = "text/javascript"}));
            Add("script:src", AbbrevationGenerator.SelfClosedTag("script", new { type = "text/javascript", src = "" }));
            Add("noscript", AbbrevationGenerator.Tag("noscript"));
        }

        private void SetupDocumentMetaData()
        {
            Add("head", AbbrevationGenerator.Tag("head"));
            Add("title", AbbrevationGenerator.Tag("title"));
            Add("base", AbbrevationGenerator.SelfClosedTag("base", new {href = ""}));
            Add("link", AbbrevationGenerator.SelfClosedTag("link"));
            Add("link:css",
                AbbrevationGenerator.SelfClosedTag("link", new
                                                       {
                                                           rel = "stylesheet",
                                                           type = "text/css",
                                                           href = "style.css",
                                                           media = "all"
                                                       }));
            Add("link:print",
                AbbrevationGenerator.SelfClosedTag("link", new
                                                       {
                                                           rel = "stylesheet",
                                                           type = "text/css",
                                                           href = "print.css",
                                                           media = "print"
                                                       }));
            Add("link:favicon",
                AbbrevationGenerator.SelfClosedTag("link", new
                                                       {
                                                           rel = "shortcut icon",
                                                           type = "image/x-icon",
                                                           href = "favicon.ico"
                                                       }));
            Add("link:touch",
                AbbrevationGenerator.SelfClosedTag("link", new {rel = "apple-touch-icon", href = "favicon.png"}));
            Add("link:rss",
                AbbrevationGenerator.SelfClosedTag("link", new
                                                       {
                                                           rel = "alternate",
                                                           type = "application/rss+xml",
                                                           title = "RSS",
                                                           href = "rss.xml"
                                                       }));
            Add("link:atom",
                AbbrevationGenerator.SelfClosedTag("link", new
                                                       {
                                                           rel = "alternate",
                                                           type = "application/atom+xml",
                                                           title = "Atom",
                                                           href = "atom.xml"
                                                       }));
            Add("meta", AbbrevationGenerator.SelfClosedTag("meta"));
            Add("meta:utf",
                AbbrevationGenerator.SelfClosedTag("meta", new
                                                       {
                                                           httpEquiv = "Content-Type",
                                                           content = "text/xml;charset=UTF-8"
                                                       }));
            Add("meta:win",
                AbbrevationGenerator.SelfClosedTag("meta", new
                                                       {
                                                           httpEquiv = "Content-Type",
                                                           content = "text/xml;charset=Win-1251"
                                                       }));
            Add("meta:compat",
                AbbrevationGenerator.SelfClosedTag("meta", new {httpEquiv = "X-UA-Compatible", content = "IE=7"}));
            Add("style", AbbrevationGenerator.Tag("style", new {type = "text/css"}));
        }

        private void SetupRootElements()
        {
            Add("html:xml", "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"\"${id}${class}>${child}</html>");
            Add("html:4t",
                @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd"">
<html lang="""">
<head>
    <title></title>
    <meta http-equiv=""Content-Type"" content=""text/html;charset=UTF-8"">
</head>
<body>
    ${child}
</body>
</html>");
            Add("html:4s",
                @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01//EN"" ""http://www.w3.org/TR/html4/strict.dtd"">
<html lang="""">
<head>
    <title></title>
    <meta http-equiv=""Content-Type"" content=""text/html;charset=UTF-8"">
</head>
<body>
    ${child}
</body>
</html>");
            Add("html:xt",
                @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
<html xmlns=""http://www.w3.org/1999/xhtml"" xml:lang="""">
<head>
    <title></title>
    <meta http-equiv=""Content-Type"" content=""text/html;charset=UTF-8""/>
</head>
<body>
    ${child}
</body>
</html>");
            Add("html:xs",
                @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">
<html xmlns=""http://www.w3.org/1999/xhtml"" xml:lang="""">
<head>
    <title></title>
    <meta http-equiv=""Content-Type"" content=""text/html;charset=UTF-8""/>
</head>
<body>
    ${child}
</body>
</html>");
            Add("html:xxs",
                @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.1//EN"" ""http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"">
<html xmlns=""http://www.w3.org/1999/xhtml"" xml:lang="""">
<head>
    <title></title>
    <meta http-equiv=""Content-Type"" content=""text/html;charset=UTF-8""/>
</head>
<body>
    ${child}
</body>
</html>");
            Add("html:5", @"<!DOCTYPE HTML>
<html lang="""">
<head>
    <title></title>
    <meta charset=""UTF-8"">
</head>
<body>
    ${child}
</body>
</html>");
        }
    }
}