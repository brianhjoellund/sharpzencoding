﻿namespace SharpZenCoding.Abbrevations
{
    using System;
    using System.Reflection;
    using System.Text;

    static class AbbrevationGenerator
    {
        public static string Tag(string tagName, object attributes = null)
        {
            return TagInternal(tagName, GetAttributes(attributes));
        }

        private static string GetAttributes(object attributes)
        {
            if (attributes == null)
                return String.Empty;

            StringBuilder sb = new StringBuilder();
            Type type = attributes.GetType();
            
            foreach (PropertyInfo info in type.GetProperties())
                sb.AppendFormat(" {0}=\"{1}\"", FormatAttributeName(info.Name), info.GetValue(attributes, null));

            return sb.ToString();
        }

        private static string FormatAttributeName(string attributeName)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in attributeName)
            {
                if (Char.IsUpper(c))
                    sb.Append("-");

                sb.Append(Char.ToLower(c));
            }

            return sb.ToString();
        }

        private static string TagInternal(string tagName, string attributeString)
        {
            return String.Format("<{0}{1}${{id}}${{class}}${{attributes}}>${{child}}</{0}>", tagName, attributeString);
        }

        public static string SelfClosedTag(string tagName, object attributes = null)
        {
            return SelfClosedTagInternal(tagName, GetAttributes(attributes));
        }

        private static string SelfClosedTagInternal(string tagName, string attributeString)
        {
            return String.Format("<{0}{1}${{id}}${{class}}${{attributes}}>", tagName, attributeString);
        }
    }
}
