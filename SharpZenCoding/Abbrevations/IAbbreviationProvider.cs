﻿namespace SharpZenCoding.Abbrevations
{
    using System.Collections.Generic;

    interface IAbbreviationProvider : IDictionary<string, string>
    {
    }
}
