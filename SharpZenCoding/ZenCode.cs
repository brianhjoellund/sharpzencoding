﻿namespace SharpZenCoding
{
    using System.Collections.Generic;
    using Core;

    public static class ZenCode
    {
        private static Parser parser;

        private static Parser Parser
        {
            get
            {
                if(parser == null)
                    parser = new Parser();

                return parser;
            }
        }

        public static ParseResults Parse(string zenCode, IEnumerable<string> input = null)
        {
            return Parser.Parse(zenCode, input);
        }
    }
}