﻿namespace SharpZenCoding.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using System.Text.RegularExpressions;
    using Core;

    class HtmlOutputSerializer : IOutputSerializer<string>
    {
        private static readonly Regex metaTagRegex = new Regex(@"\$\{(?<name>\w+)}");

        public string Serialize(INode node)
        {
            return SerializeInternal(node, 0, node.TopMostContext.Input);
        }

        private string SerializeInternal(INode node, int indent, IEnumerable<string> input)
        {
            var returnString = new StringBuilder();
            int count = node.Count;
            List<string> inputList = null;

            if(input != null)
                inputList = new List<string>(input);

            if (count < 1)
                count = 1;

            if (Providers.Abbrevations.ContainsKey(node.Name))
            {
                string providerString = Providers.Abbrevations[node.Name];

                for (int i = 0; i < count; ++i)
                {
                    int itemNumber = i + 1;

                    returnString.Append(new string('\t', indent) +
                                        metaTagRegex.Replace(providerString, m => { switch (m.Groups["name"].Value)
                                        {
                                            case "class":
                                                return FormatAsClassNames(node.ClassNames, itemNumber);
                                            case "child":
                                                return (node.ReceiveInput && inputList != null ? inputList[itemNumber - 1] : String.Empty) + FormatChildren(node, indent + 1, input);
                                            case "attributes":
                                                return FormatAttributes(node.Attributes);
                                            case "id":
                                                return FormatAsId(node.Id, itemNumber);
                                            default:
                                                return String.Empty;
                                        }
                                        }) + Environment.NewLine);
                }
            }
            else
            {
                for (int i = 0; i < count; ++i)
                    returnString.AppendFormat("{0}<{1}{7}{2}{5}>{4}{3}</{1}>{6}",
                        new string('\t', indent),
                        node.Name,
                        FormatAsClassNames(node.ClassNames, i + 1),
                        FormatChildren(node, indent + 1, input),
                        node.ReceiveInput&&inputList!=null?inputList[i]:String.Empty,
                        FormatAttributes(node.Attributes),
                        Environment.NewLine,
                        FormatAsId(node.Id, i + 1));
            }

            string temp = returnString.ToString();

            if (Regex.Matches(temp, Environment.NewLine).Count == 1)
                temp = temp.TrimEnd();

            return temp;
        }

        private string FormatAttributes(IDictionary<string, string> attributes)
        {
            if (attributes == null || attributes.Count == 0)
                return String.Empty;

            var attributesString = new StringBuilder();

            foreach (var attribute in attributes)
                attributesString.AppendFormat(" {0}=\"{1}\"", attribute.Key, attribute.Value);

            return attributesString.ToString();
        }

        private string FormatChildren(INode node, int indent, IEnumerable<string> input)
        {
            var childrenBuilder = new StringBuilder();

            foreach (INode child in node)
                childrenBuilder.Append(Environment.NewLine + SerializeInternal(child, indent, input));

            if(childrenBuilder.Length > 0)
                return childrenBuilder + new string('\t', Math.Max(0, indent - 1));

            if (!node.ReceiveInput)
                return "$";

            return String.Empty;
        }

        private static string FormatAsId(string id, int index)
        {
            if (String.IsNullOrEmpty(id))
                return String.Empty;

            return String.Format(" id=\"{0}\"", id.Replace("$", index.ToString(CultureInfo.InvariantCulture)));
        }

        private static string FormatAsClassNames(IEnumerable<string> classNames, int index)
        {
            var classNamesList = new List<string>(classNames);
            if (classNames == null || classNamesList.Count == 0)
                return String.Empty;

            for (int i = 0; i < classNamesList.Count; i++)
                classNamesList[i] = classNamesList[i].Replace("$", index.ToString(CultureInfo.InvariantCulture));
            
            return String.Format(" class=\"{0}\"", String.Join(" ", classNamesList.ToArray()));
        }
    }
}
