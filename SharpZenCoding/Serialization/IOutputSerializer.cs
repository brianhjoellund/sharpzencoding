﻿namespace SharpZenCoding.Serialization
{
    using Core;

    interface IOutputSerializer<out TOutputType>
    {
        TOutputType Serialize(INode node);
    }
}