namespace SharpZenCoding.Snippets
{
    using System.Collections.Generic;

    public interface ISnippetProvider : ICollection<ISnippet>
    {
    }
}