namespace SharpZenCoding.Snippets
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    class SnippetProvider : ISnippetProvider
    {
        private const double GrowFactor = 0.6;
        private const int InitialBucketSize = 5;

        private ISnippet[] snippets;
        private int count;

        public SnippetProvider()
        {
            snippets = new ISnippet[InitialBucketSize];
        }

        public IEnumerator<ISnippet> GetEnumerator()
        {
            for (int i = 0; i < count; ++i)
                yield return snippets[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(ISnippet item)
        {
            if (snippets.Length == count)
            {
                ISnippet[] newArray = new ISnippet[(int)(Math.Ceiling(count*GrowFactor))];
                snippets.CopyTo(newArray, 0);
            }

            snippets[count] = item;
            count++;
        }

        public void Clear()
        {
            snippets = new ISnippet[InitialBucketSize];
        }

        public bool Contains(ISnippet item)
        {
            foreach (ISnippet snippet in snippets)
                if (snippet == item)
                    return true;

            return false;
            //return snippets.Contains(item);
        }

        public void CopyTo(ISnippet[] array, int arrayIndex)
        {
            Array.Copy(snippets, 0, array, arrayIndex, count);
        }

        public bool Remove(ISnippet item)
        {
            int length = snippets.Length;
            List<ISnippet> newSnippets = new List<ISnippet>();

            foreach (ISnippet snippet in snippets)
                if (snippet != item)
                    newSnippets.Add(snippet);
            
            snippets = newSnippets.ToArray();
            return snippets.Length != length;
        }

        public int Count
        {
            get { return count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }
    }
}