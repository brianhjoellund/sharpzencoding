﻿namespace SharpZenCoding
{
    using System;
    using Ninject;

    static class IoC
    {
        private class ZenCodingKernel : StandardKernel
        {
            public ZenCodingKernel() : base(new ZenCodeInjectionModule()) { }
        }

        private readonly static Lazy<ZenCodingKernel> Kernel = new Lazy<ZenCodingKernel>(() => new ZenCodingKernel());

        public static T Get<T>()
        {
            return Kernel.Value.Get<T>();
        }
    }
}
