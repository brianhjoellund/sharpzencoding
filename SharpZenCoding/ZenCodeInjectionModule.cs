﻿namespace SharpZenCoding
{
    using Abbrevations;
    using Core;
    using Ninject.Modules;
    using Serialization;
    using Snippets;

    class ZenCodeInjectionModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IOutputSerializer<string>>().To<HtmlOutputSerializer>();
            Bind<IAbbreviationProvider>().To<AbbreviationProvider>();
            Bind<ISnippetProvider>().To<SnippetProvider>();
            Bind<IContext>().To<Context>();
            Bind<INode>().To<Node>();
            Bind<ISnippet>().To<Snippet>();
        }
    }
}
